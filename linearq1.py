# File linearq1.py  - give to the students.

from linearhash import LinearQHashTable
from person import Person

def main():
   print ('Run # 1')
   table = LinearQHashTable(5, deleted = -3)
   table.insert(8)
   table.insert(18)
   table.insert(13)
   table.insert(28)
   table.insert(43)
   table.insert(57)
   print (table)
   
   print ('Run # 2')
   table = LinearQHashTable(5, empty = 'Nothing!', deleted = 'Gone!')
   table.insert(Person('Joe', 15))
   table.insert(Person('Jill', 3))
   table.insert(Person('Bill', 1))
   table.insert(Person('Maude', 19))
   table.insert(Person('Lennie', 23))
   print (table)

   print ('Run # 3')
   table = LinearQHashTable(5)
   table.insert(8)
   table.insert(18)
   table.insert(13)
   table.insert(28)
   table.insert(43)
   table.delete(18)
   table.insert(57)
   print (table)
   print (table.retrieve(15))
   print (table.retrieve(43))
   print (table)
   
   print ('Run # 4')
   table = LinearQHashTable(7)
   table.insert(15)
   table.insert(3)
   table.insert(1)
   table.insert(19)
   table.insert(23)
   table.insert(30)
   table.insert(37)
   table.insert(44)
   table.insert(51)
   table.insert(58)
   print (table)
   
   print ('Run # 5')
   table = LinearQHashTable(5, empty = 'Nothing!', deleted = 'Gone!')
   table.insert(Person('Joe', 15))
   table.insert(Person('Jill', 3))
   table.insert(Person('Bill', 1))
   table.insert(Person('Maude', 19))
   table.delete(Person(" ", 3))
   print (table)
   table.insert(Person('Lennie', 23))
   print (table)
   print (table.retrieve(Person(" ", 19)))
   print (table.retrieve(Person(" ", 45)))
   print (table.update(Person('Billy Joe Bob', 1)))
   print (table.update(Person('Susie', 45)))
   print (table)
   
   h = LinearQHashTable(70, -3, -5)
   # Could print an error message.
   
if __name__ == '__main__': main()

''' This is what the output should look like:
[evaluate Linearq1.py]
Run # 1
The size of the hash table is 5
0: 13
1: 18
2: 43
3: 8
4: 28

Run # 2
The size of the hash table is 5
0: Name: Joe Id: 15 
1: Name: Bill Id: 1 
2: Name: Lennie Id: 23 
3: Name: Jill Id: 3 
4: Name: Maude Id: 19 

Run # 3
The size of the hash table is 5
0: 13
1: 57
2: 43
3: 8
4: 28

None
43
The size of the hash table is 5
0: 13
1: 57
2: 43
3: 8
4: 28

Run # 4
The size of the hash table is 7
0: 37
1: 15
2: 1
3: 3
4: 23
5: 19
6: 30

Run # 5
The size of the hash table is 5
0: Name: Joe Id: 15 
1: Name: Bill Id: 1 
3: Gone!
4: Name: Maude Id: 19 

The size of the hash table is 5
0: Name: Joe Id: 15 
1: Name: Bill Id: 1 
3: Name: Lennie Id: 23 
4: Name: Maude Id: 19 

Name: Maude Id: 19 
None
Name: Billy Joe Bob Id: 1 
None
The size of the hash table is 5
0: Name: Joe Id: 15 
1: Name: Billy Joe Bob Id: 1 
3: Name: Lennie Id: 23 
4: Name: Maude Id: 19 

Invalid table size of 70 -- not a prime.

'''
