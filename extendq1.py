# File extendq1.py

from linearhash import ExtendedQHashTable
from person import Person

def main():
   print ('Run # 1')
   table = ExtendedQHashTable(7)
   table.insert(Person('Joe', 15))
   table.insert(Person('Jill', 3))
   table.insert(Person('Bill', 1))
   table.insert(Person('Maude', 19))
   table.insert(Person('Lennie', 23))
   table.insert(Person('Walt', 30))
   table.insert(Person('Donald', 19))
   table.insert(Person('Sue', 37))
   table.insert(Person('Sharon', 44))
   print (table)

   print ('Run # 2')
   table = ExtendedQHashTable(7, deleted = -3)
   table.insert(15)
   table.insert(3)
   table.insert(1)
   table.insert(19)
   table.delete(15)
   table.insert(23)
   table.insert(30)
   table.insert(37)
   table.insert(44)
   table.delete(23)
   table.insert(51)
   print( table )

   print ('Run # 3')
   table = ExtendedQHashTable(7)
   table.insert(15)
   table.insert(3)
   table.insert(1)
   table.insert(19)
   table.delete(1)
   table.insert(23)
   print (table)
   print (table.retrieve(15))
   print (table.retrieve(45))
   print (table)

   print ('Run # 4')
   table = ExtendedQHashTable(7)
   table.insert(15)
   table.insert(3)
   table.insert(1)
   table.insert(19)
   table.insert(23)
   table.insert(30)
   table.insert(37)
   table.delete(19)
   table.delete(15)
   table.insert(44)
   table.insert(51)
   table.insert(58)
   print (table)
   
   print ('Run # 5')
   table = ExtendedQHashTable(7)
   table.insert(Person('Joe', 15))
   table.insert(Person('Jill', 3))
   table.insert(Person('Bill', 1))
   table.insert(Person('Maude', 19))
   print (table.delete(Person("Fred",3)))
   print (table.insert(Person('Lennie', 23)))
   print (table)
   print (table.retrieve(Person(" ", 19)))
   print (table.retrieve(Person(" ",45)))
   print (table.update(Person('Billy Joe Bob', 1)))
   print (table.update(Person('Susie', 45)))
   print (table)
   print (table.delete(Person(" ",33)))
   
   table = ExtendedQHashTable(17)
   table = ExtendedQHashTable(15)
   

if __name__ == '__main__': main()

''' The output should look like the following:
Run # 1
The size of the hash table is 7
0: Name: Sue Id: 37 
1: Name: Joe Id: 15 
2: Name: Bill Id: 1 
3: Name: Jill Id: 3 
4: Name: Walt Id: 30 
5: Name: Maude Id: 19 
6: Name: Lennie Id: 23 

Run # 2
The size of the hash table is 7
0: 44
1: 51
2: 1
3: 3
4: 37
5: 19
6: 30

Run # 3
The size of the hash table is 7
1: 15
2: 23
3: 3
5: 19

15
None
The size of the hash table is 7
1: 15
2: 23
3: 3
5: 19

Run # 4
The size of the hash table is 7
0: 37
1: 44
2: 1
3: 3
4: 30
5: 51
6: 23

Run # 5
Name: Jill Id: 3 
Name: Lennie Id: 23 
The size of the hash table is 7
1: Name: Joe Id: 15 
2: Name: Bill Id: 1 
3: Name: Lennie Id: 23 
5: Name: Maude Id: 19 

Name: Maude Id: 19 
None
Name: Billy Joe Bob Id: 1 
None
The size of the hash table is 7
1: Name: Joe Id: 15 
2: Name: Billy Joe Bob Id: 1 
3: Name: Lennie Id: 23 
5: Name: Maude Id: 19 

None
Need a prime table size of the form  4*k + 3, not 17.
Need a prime table size of the form  4*k + 3, not 15.

'''
